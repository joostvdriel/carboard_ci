<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('ion_auth');
        if ( $this->ion_auth->logged_in() == false) {
            // redirect to login view
            redirect(base_url() .'auth/login/');
        }
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function view( $page = 'dashboard' )
	{

		// $this->load->view('welcome_message');
        if ( ! file_exists(APPPATH.'/views/backend/pages/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/page/'.$page, $data);
        $this->load->view('backend/templates/footer', $data);

	}

    public function dashboard( ){

        $data['title'] = '';

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/dashboard', $data);
        $this->load->view('backend/templates/footer', $data);
    }

    public function settings( ){

        $data['title'] = 'Instellingen';

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/settings', $data);
        $this->load->view('backend/templates/footer', $data);
    }

}
