<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyCarboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('ion_auth');
        if ( $this->ion_auth->logged_in() == false) {
            // redirect to login view
            redirect(base_url() .'auth/login/');
        }

        // load car model
        $this->load->model('car_model');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function cars( $page = 'dashboard' )
	{

        $data['title'] = ucfirst($page); // Capitalize the first letter

        // get the cars of this user
        $user_cars = $this->car_model->get_cars_by_user_id( $this->ion_auth->user()->row()->id );

        $this->load->view('backend/templates/header', $data);

        if( empty($user_cars) ){
            $this->load->view('backend/templates/mycarboard/no_cars.php', $data);
        }
        else{
            // load cars into view
            $data['user_cars'] = $user_cars;

            $this->load->view('backend/templates/mycarboard/cars.php', $data);
        }

        $this->load->view('backend/templates/footer', $data);

	}

    public function costs( ){

        $data['title'] = '';

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/dashboard', $data);
        $this->load->view('backend/templates/footer', $data);
    }

    public function tanking( ){

        $data['title'] = 'Instellingen';

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/settings', $data);
        $this->load->view('backend/templates/footer', $data);
    }

    public function maintenance( ){

        $data['title'] = 'Instellingen';

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/settings', $data);
        $this->load->view('backend/templates/footer', $data);
    }

    public function export( ){

        $data['title'] = 'Instellingen';

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/settings', $data);
        $this->load->view('backend/templates/footer', $data);
    }

    public function add_car( ){

        // load libs
        $this->load->library('form_validation');

        $data['title'] = 'Auto toevoegen';

        //validate form input
        $this->form_validation->set_rules('kentekenInput', 'kenteken verplicht' , 'required');

        if ($this->form_validation->run() == true)
        {
            $car['car_license'] = strtolower($this->input->post('kentekenInput'));
            $car['car_owner_user_id'] = $this->ion_auth->user()->row()->id;
        }

        if ($this->form_validation->run() == true && $this->car_model->insert_car($car)){
            //check to see if we are creating the user
            //redirect them back to the admin page
            //$this->session->set_flashdata('message', $this->car_model->messages());
            redirect("/carboard/mycars/", 'refresh');
        }
        else
        {
            //display the create user form
            //set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['first_name'] = array(
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $this->data['last_name'] = array(
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['company'] = array(
                'name'  => 'company',
                'id'    => 'company',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('company'),
            );
            $this->data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone'),
            );
            $this->data['password'] = array(
                'name'  => 'password',
                'id'    => 'password',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['password_confirm'] = array(
                'name'  => 'password_confirm',
                'id'    => 'password_confirm',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            );

           //  $this->_render_page('auth/create_user', $this->data);
            echo "invalid";
        }

        $this->load->view('backend/templates/header', $data);
        $this->load->view('backend/templates/mycarboard/add_car', $data);
        $this->load->view('backend/templates/footer', $data);

    }

    function edit_car($id){

        if( !empty($id) && is_numeric($id) ){

            // get car data
            $user_car = $this->car_model->get_car_by_id( $id, $this->ion_auth->user()->row()->id );

            // load car into view
            $data['user_car'] = $user_car;

            $this->load->view('backend/templates/header', $data);
            $this->load->view('backend/templates/mycarboard/edit_car.php', $data);
            $this->load->view('backend/templates/footer', $data);

        }


    }

}
