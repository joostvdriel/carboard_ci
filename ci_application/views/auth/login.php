<html>
<head>
    <title>CodeIgniter Tutorial</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- page styling -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/auth/login.css') ?>">

</head>
<body>
    <div class="container">
        <form class="form-signin" method="post" action="<?php echo base_url('auth/login') ?> ">
            <h2 class="form-signin-heading">
                <?php echo lang('login_heading');?>
            </h2>
            <div id="infoMessage">
                <?php echo $message;?>
            </div>
            <label for="inputEmail" class="sr-only">
                <?php echo lang('login_identity_label', 'identity');?>
            </label>
            <input type="email" id="inputEmail" name="inputEmail" class="form-control"
                   placeholder="Email address" required autofocus ">
            <label for="inputPassword" class="sr-only">
                <?php echo lang('login_password_label', 'password');?>
            </label>
            <input type="password" id="inputPassword" name="inputPassword" class="form-control"
                   placeholder="Password" required>
            <div class="checkbox" id="rememberMe">
                <label for="rememberMe">
                    <input type="checkbox" value="remember-me">
                    <?php echo lang('login_remember_label', 'remember');?>
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">
                <?php echo lang('login_submit_btn');?>
            </button>
        </form>
        <p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>

    </div> <!-- /container -->
</body>
</html>
