<html>
    <head>
        <title>CodeIgniter Tutorial</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="http://getbootstrap.com/examples/dashboard/dashboard.css">

        <!-- Font awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    </head>
    <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/carboard/dashboard/">Mijn Carboard</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/carboard/dashboard/">Dashboard</a></li>
                    <li><a href="/carboard/settings/"><i class="fa fa-fw fa-gear"></i> Settings</a></li>
                    <li>

                        <a href="/auth/edit/<?php echo $this->ion_auth->user()->row()->id ?>"><i class="fa fa-user"></i> Profile</a>
                    </li>
                    <li><a href="/auth/logout/">Logout</a></li>
                </ul>
                <form class="navbar-form navbar-right">
                    <input type="text" class="form-control" placeholder="Search...">
                </form>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li><a href="/carboard/mycars/">Mijn auto's</a></li>
                    <li><a href="/carboard/mycosts/">Kosten</a></li>
                    <li><a href="/carboard/mytanking/">Tanken</a></li>
                    <li><a href="/carboard/mymaintenance/">Onderhoud</a></li>
                    <li><a href="/carboard/export/">Export</a></li>
                </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">


