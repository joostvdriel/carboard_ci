<h1 class="page-header">Nieuwe auto toevoegen</h1>
<p>Door een auto toe te voegen krijgt u inzicht in alle kosten.</p>
<div class="panel panel-default">
    <div class="panel-heading">
        Vul het onderstaande formulier in om uw auto toe te voegen
    </div>
    <div class="">
        <?php if ( !empty($message) ) echo $message ?>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <form role="form" method="post">
                    <div class="form-group">
                        <label for="kentekenInput">Kenteken</label>
                        <input id="kentekenInput" name="kentekenInput" class="form-control" placeholder="">
                    </div>
                </form>
            </div>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->
