<h1 class="page-header">Bestaande auto wijzigen</h1>
<p>Wijzig de gegevens van uw auto.</p>
<div class="panel panel-default">
    <div class="panel-heading">
        Breng uw wijzigingen aan en sla ze op
    </div>
    <div class="">
        <?php if ( !empty($message) ) echo $message ?>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <form role="form" method="post">
                    <div class="form-group">
                        <label for="kentekenInput">Kenteken</label>
                        <input id="kentekenInput" name="kentekenInput"
                                class="form-control" placeholder="" value="<?php echo $user_car->car_license ?>">

                    </div>
                </form>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".get-car-data">
                    RDW gegevens ophalen
                </button>
            </div>
        </div>
        <!-- /.row (nested) -->
    </div>
    <!-- /.panel-body -->
</div>
<!-- /.panel -->

<!-- Overlay panel -->
<div class="modal fade get-car-data" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Gegevens ophalen voor uw auto.</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <span class="fa fa-circle-o-notch fa-spin"></span>
                        Verbinden met RDW.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.overlay panel -->
