<h1 class="page-header">Mijn auto's <a href="/carboard/mycars/add/" class="btn btn-primary">Auto toevoegen</a></h1>


<?php foreach ($user_cars as $car): ?>
    <div class="col-lg-4">
        <div class="well">
            <h4><?php echo strtoupper($car->car_license) ?></h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
            <a href="/carboard/mycars/edit/<?php echo $car->car_id ?>" class="btn btn-primary">Wijzigen</a>
        </div>
    </div>
<?php endforeach ?>

