<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Car_model extends CI_Model{

    public $car_id;
    public $car_license;
    public $car_brand;
    public $car_brand_model;
    public $car_owner_user_id;

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();

    }

    public function get_cars_by_user_id($user_id){

        // check input
        if( !empty($user_id) && is_numeric($user_id)){

            // get car by car id and user id
            $query = $this->db->get_where('car', array('car_owner_user_id' => $user_id));

            // return result
            return $query->result();
        }

        return false;

    }

    /*
     * Gets car by it's id
     * Needs a user id to check if it's really the users car
     *
     */
    public function get_car_by_id($car_id, $user_id){

        // check input
        if( !empty($car_id) && is_numeric($car_id) && !empty($user_id) && is_numeric($user_id)){

            // get car by car id and user id
            $query = $this->db->get_where('car', array('car_id' => $car_id, 'car_owner_user_id' => $user_id));

            // return result
            return $query->row();
        }

        return false;
    }

    public function insert_car($car){

        // check input
        if( !empty($car) ){

            // be sure to check the input?

            // insert the car
            $this->db->insert('car', $car);

            // return result
            return $this->db->insert_id();
        }

        return false;

    }

    public function update_car(){

    }

    public function remove_car(){

    }



}


