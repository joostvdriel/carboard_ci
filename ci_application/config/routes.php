<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


// Backend rules
$route['auth']              = 'auth/login';             // login
$route['auth/login']        = 'auth/login';             // login
$route['auth/logout']       = 'auth/logout';            // logout
$route['auth/index']        = 'auth/index';             // list all users
$route['auth/edit/(:num)']  = 'auth/edit_user/$1';      // edit user profile

$route['carboard']               = 'backend/view/$1';
$route['carboard/dashboard']     = 'backend/dashboard';
$route['carboard/settings']      = 'backend/settings';

$route['carboard/mycars']        = 'mycarboard/cars';
$route['carboard/mycars/add']    = 'mycarboard/add_car';
$route['carboard/mycars/edit/(:num)'] = 'mycarboard/edit_car/$1';


$route['carboard/mycosts']       = 'mycarboard/costs';
$route['carboard/mytanking']     = 'mycarboard/tanking';
$route['carboard/mymaintenance'] = 'mycarboard/maintenance';
$route['carboard/export']        = 'mycarboard/export';

$route['carboard/api/rdw/getcardata/(:any)'] = 'rdw/getcardata/$1';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['default_controller'] = 'pages/view';
$route['(:any)'] = 'pages/view/$1';

